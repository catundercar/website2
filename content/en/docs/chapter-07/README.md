---
title: "Delete one or more records"
description: ""
lead: ""
date: 2022-04-12T20:11:31+08:00
lastmod: 2022-04-12T20:11:31+08:00
draft: false
images: []
menu:
  docs:
    parent: "chapter-07"
weight: 4100
toc: true
---

## 7.Delete one or more records
Delete one or more records

* delete by id

```Go
affected, err := engine.ID(1).Delete(&User{})
```

* delete by other conditions

```Go
affected, err := engine.Delete(&User{Name:"xlw"})
```
