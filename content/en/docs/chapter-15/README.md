---
title: "FAQ"
description: ""
lead: ""
date: 2022-04-12T20:11:31+08:00
lastmod: 2022-04-12T20:11:31+08:00
draft: false
images: []
menu:
  docs:
    parent: "chapter-15"
weight: 610
toc: true
---

# FAQ

* How the xorm tag use both with json?

  Use space.

```Go
type User struct {
    Name string `json:"name" xorm:"name"`
}
```

* Does xorm support composite primary key?

  Yes. You can use pk tag. All fields have tag `pk` will as one primary key by fields order on struct. When use, you can use `xorm.PK{1, 2}`. For example:

```go
    engine.ID(xorm.PK{1, 2})
```

* How to use join？

  We can use `Join()` and `extends` tag to do join operation. For example:

```go
type Userinfo struct {
    Id int64
    Name string
    DetailId int64
}

type Userdetail struct {
    Id int64
    Gender int
}

type User struct {
    Userinfo `xorm:"extends"`
    Userdetail `xorm:"extends"`
}

var users = make([]User, 0)
err := engine.Table(&Userinfo{}).Join("LEFT", "userdetail", "userinfo.detail_id = userdetail.id").Find(&users)

//assert(User.Userinfo.Id != 0 && User.Userdetail.Id != 0)
```

Please notice that Userinfo field on User should be before Userdetail because of the order on join SQL stsatement. If the order is wrong, the same name field may be set a wrong value and no error will be indicated.

Of course, if join statment is very long, you could directly use Sql():

```go
err := engine.SQL("select * from userinfo, userdetail where userinfo.detail_id = userdetail.id").Find(&users)

//assert(User.Userinfo.Id != 0 && User.Userdetail.Id != 0)
```

* How to set database time location?

```Go
location, err = time.LoadLocation("Asia/Shanghai")
engine.TZLocation = location
```
